package com.aswdc_matrimony.view;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc_matrimony.R;
import com.aswdc_matrimony.adapter.CityListAdapter;
import com.aswdc_matrimony.dbhandler.DatabaseHandler;
import com.aswdc_matrimony.model.CityModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddUserActivity extends AppCompatActivity {

    EditText etUserName;
    Spinner spCity;
    EditText etDateOfBirth;
    RadioButton rbMale;
    RadioButton rbFeamle;
    RadioButton rbOther;
    EditText etPhone;
    EditText etOccupation;
    Button btnSubmit;
    DatabaseHandler databaseHandler;
    ArrayList<CityModel> cityList = new ArrayList<>();

    boolean isCalendarOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        initViewReference();
    }

    void initViewReference() {
        etUserName = findViewById(R.id.etUserName);
        spCity = findViewById(R.id.spCity);
        etDateOfBirth = findViewById(R.id.etDateOfBirth);
        rbMale = findViewById(R.id.rbMale);
        rbFeamle = findViewById(R.id.rbFeamle);
        rbOther = findViewById(R.id.rbOther);
        etPhone = findViewById(R.id.etPhone);
        etOccupation = findViewById(R.id.etOccupation);
        btnSubmit = findViewById(R.id.btnSubmit);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());

        etDateOfBirth.setText(formattedDate);

        etDateOfBirth.setOnClickListener(v -> {
            if (!isCalendarOpen) {
                isCalendarOpen = true;
                showDatePickerDialog();
            }
        });

        databaseHandler = new DatabaseHandler(this);
        cityList = databaseHandler.getCityList();
        spCity.setAdapter(new CityListAdapter(this, cityList));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation())
                    insertIntoUserTable();
            }
        });
    }

    void insertIntoUserTable() {
        ContentValues cv = new ContentValues();
        cv.put("UserName", etUserName.getText().toString());
        cv.put("CityID", cityList.get(spCity.getSelectedItemPosition()).getCityId());
        cv.put("DOB", etDateOfBirth.getText().toString());
        cv.put("Gender", rbMale.isChecked() ? 0 : (rbFeamle.isChecked() ? 1 : 2));
        cv.put("ContactNo", etPhone.getText().toString());
        cv.put("Occupation", etOccupation.getText().toString());
        if (databaseHandler.insertUserDetail(cv) > 0) {
            Toast.makeText(this, "User Inserted Successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Error Occured", Toast.LENGTH_SHORT).show();
        }
    }

    void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    etDateOfBirth.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    isCalendarOpen = false;
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    boolean checkValidation() {
        if (TextUtils.isEmpty(etUserName.getText().toString())) {
            etUserName.setError("Enter User Name");
            return false;
        }
        if (TextUtils.isEmpty(etPhone.getText().toString())) {
            etPhone.setError("Enter Contact No");
            return false;
        }
        if (etPhone.getText().toString().length() < 10) {
            etPhone.setError("Enter Valid Contact No");
            return false;
        }
        return true;
    }
}