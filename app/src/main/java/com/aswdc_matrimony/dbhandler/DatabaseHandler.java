package com.aswdc_matrimony.dbhandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_matrimony.model.CityModel;
import com.aswdc_matrimony.model.UserModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteAssetHelper {

    public static final String DB_NAME = "matrimony.db";
    public static final int DB_VERSION = 1;

    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    public ArrayList<CityModel> getCityList() {
        ArrayList<CityModel> cityList = new ArrayList<>();
        String query = "SELECT * FROM Tbl_City";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            CityModel cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex("CityID")));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex("CityName")));
            cityList.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cityList;
    }

    public ArrayList<UserModel> getUserList() {
        ArrayList<UserModel> userList = new ArrayList<>();
        String query = "SELECT * FROM Tbl_User";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            UserModel model = new UserModel();
            model.setUserID(cursor.getInt(cursor.getColumnIndex("UserID")));
            model.setCityID(cursor.getInt(cursor.getColumnIndex("CityID")));
            model.setContactNo(cursor.getString(cursor.getColumnIndex("ContactNo")));
            model.setDOB(cursor.getString(cursor.getColumnIndex("DOB")));
            model.setGender(cursor.getInt(cursor.getColumnIndex("Gender")));
            model.setOccupation(cursor.getString(cursor.getColumnIndex("Occupation")));
            model.setUserName(cursor.getString(cursor.getColumnIndex("UserName")));
            userList.add(model);
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return userList;
    }

    public long insertUserDetail(ContentValues cv) {
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert("Tbl_User", null, cv);
        db.close();
        return id;
    }

    public int deleteUser(int userID) {
        SQLiteDatabase db = getWritableDatabase();
        int id = db.delete("Tbl_User", "UserID = ?", new String[]{String.valueOf(userID)});
        db.close();
        return id;
    }
}