package com.aswdc_matrimony.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc_matrimony.R;
import com.aswdc_matrimony.model.CityModel;

import java.util.ArrayList;

public class CityListAdapter extends BaseAdapter {

    ArrayList<CityModel> cityList;
    Context context;

    public CityListAdapter(Context context, ArrayList<CityModel> cityList) {
        this.cityList = cityList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_row_city_item, null);
        TextView tvName = v.findViewById(R.id.tvCityName);
        tvName.setText(cityList.get(position).getCityName());
        return v;
    }
}