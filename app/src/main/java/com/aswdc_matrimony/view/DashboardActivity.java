package com.aswdc_matrimony.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.aswdc_matrimony.R;

public class DashboardActivity extends AppCompatActivity {

    CardView cvAddUser;
    CardView cvListUser;
    CardView cvSearchUser;
    CardView cvFavoriteUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        cvAddUser = findViewById(R.id.cvAddUser);
        cvListUser = findViewById(R.id.cvListUser);
        cvSearchUser = findViewById(R.id.cvSearchUser);
        cvFavoriteUser = findViewById(R.id.cvFavoriteUser);

        cvAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, AddUserActivity.class);
                startActivity(intent);
            }
        });

        cvListUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, UserListActivity.class);
                startActivity(intent);
            }
        });

        cvSearchUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this, "SEARCH User Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        cvFavoriteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this, "FAVORITE User Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}