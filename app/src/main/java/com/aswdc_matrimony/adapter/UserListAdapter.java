package com.aswdc_matrimony.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_matrimony.OnDeleteUser;
import com.aswdc_matrimony.R;
import com.aswdc_matrimony.dbhandler.DatabaseHandler;
import com.aswdc_matrimony.model.UserModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {
    Context context;
    ArrayList<UserModel> userList;
    DatabaseHandler dbHandler;

    OnDeleteUser onDeleteUser;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnDeleteUser onDeleteUser) {
        this.context = context;
        this.userList = userList;
        dbHandler = new DatabaseHandler(context);
        this.onDeleteUser = onDeleteUser;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.tvDob.setText(userList.get(position).getDOB());
        holder.tvContactNo.setText(userList.get(position).getContactNo());
        holder.tvUserName.setText(userList.get(position).getUserName());

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage("Are you sure want to delete this user?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (dbHandler.deleteUser(userList.get(position).getUserID()) > 0) {
                                    if (onDeleteUser != null) {
                                        onDeleteUser.onDeleteUser();
                                    }
                                }
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName;
        TextView tvContactNo;
        TextView tvDob;
        ImageView ivDelete;

        public UserViewHolder(View v) {
            super(v);
            tvUserName = v.findViewById(R.id.tvName);
            tvContactNo = v.findViewById(R.id.tvContactNo);
            tvDob = v.findViewById(R.id.tvDob);
            ivDelete = v.findViewById(R.id.ivDeleteUser);
        }
    }
}