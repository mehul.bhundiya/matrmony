package com.aswdc_matrimony.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_matrimony.OnDeleteUser;
import com.aswdc_matrimony.R;
import com.aswdc_matrimony.adapter.UserListAdapter;
import com.aswdc_matrimony.dbhandler.DatabaseHandler;
import com.aswdc_matrimony.model.UserModel;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {

    RecyclerView rcvUserList;
    EditText etSearchUser;
    DatabaseHandler dbHandler;
    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();

    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        rcvUserList = findViewById(R.id.rcvUserList);
        etSearchUser = findViewById(R.id.etSearchUser);

        dbHandler = new DatabaseHandler(this);

        userList = dbHandler.getUserList();
        tempUserList.addAll(dbHandler.getUserList());

        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));

        adapter = new UserListAdapter(this, tempUserList, new OnDeleteUser() {
            @Override
            public void onDeleteUser() {
                userList.clear();
                tempUserList.clear();
                userList = dbHandler.getUserList();
                tempUserList.addAll(dbHandler.getUserList());
                adapter.notifyDataSetChanged();
            }
        });
        rcvUserList.setAdapter(adapter);

        etSearchUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tempUserList.clear();
                for (int i = 0; i < userList.size(); i++) {
                    if (userList.get(i).getUserName().toLowerCase().contains(s.toString().toLowerCase()) || userList.get(i).getContactNo().toLowerCase().contains(s.toString().toLowerCase())
                            || userList.get(i).getDOB().toLowerCase().contains(s.toString().toLowerCase())) {
                        tempUserList.add(userList.get(i));
                    }
                }
                if (s.toString().length() == 0 && tempUserList.size() == 0) {
                    tempUserList.addAll(userList);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
